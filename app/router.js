import EmberRouter from '@ember/routing/router';
import config from 'web-app/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

// Path configuration to URL changed 
Router.map(function () {
  this.route('game', { path: '/' });
});